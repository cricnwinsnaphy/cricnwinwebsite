import React from 'react';
import { Menu, Icon, Image } from 'semantic-ui-react';

function Header() {
  return (
    <Menu
      stackable
      style={{ borderRadius: '0', marginBottom: '0', border: 'none' }}
      borderless
      size="large"
    >
      <Menu.Menu position="left" style={{ width: '11.5%', marginRight: '0 !important' }}>
        <Menu.Item name="browse" icon link>
          <Icon name="sidebar" />
        </Menu.Item>

        <Menu.Item name="reviews" link>
          cricnwin
        </Menu.Item>
      </Menu.Menu>
      <Menu.Menu position="left">
        <Menu.Item name="upcomingEvents" position="left" link>
          HOME
        </Menu.Item>
      </Menu.Menu>
      <Menu.Menu position="right">
        <Menu.Item name="layout" link style={{ paddingLeft: '5px', paddingRight: '5px' }}>
          <Icon name="grid layout" />
        </Menu.Item>
        <Menu.Item name="notification" link style={{ paddingLeft: '5px', paddingRight: '5px' }}>
          <Icon name="bell outline" />
        </Menu.Item>

        <Menu.Item name="kmmkm" link style={{ paddingLeft: '5px' }}>
          <Image
            style={{ width: '20px' }}
            size="mini"
            src="https://www.seoclerk.com/pics/558390-11FO8A1505384509.png"
            rounded
          />
        </Menu.Item>
      </Menu.Menu>
    </Menu>
  );
}

export default Header;
