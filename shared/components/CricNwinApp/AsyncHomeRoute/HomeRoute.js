import React from 'react';
import Helmet from 'react-helmet';
import { Button, Grid } from 'semantic-ui-react';

// Header..
import { Header, HomeSidebar } from './../PartialComponents';

import { Carousel } from './Partials/Carousel';
import { NewsList } from './Partials/NewsList';
import { RecentMatchList } from './Partials/RecentMatchList';

import config from '../../../../config';

function HomeRoute() {
  return (
    <div>
      <Helmet>
        <title>Home Test SEO</title>
      </Helmet>

      {/* Header */}
      <Header />

      <Grid>
        <Grid.Column width={2}>
          {/* SideBar */}
          <HomeSidebar />
        </Grid.Column>
        <Grid.Column width={12}>
          {/* Carousel */}
          <Carousel />

          {/* Recent Matches.. */}
          <RecentMatchList />

          {/* News List */}
          <NewsList />
        </Grid.Column>
        <Grid.Column width={2} />
      </Grid>
    </div>
  );
}

export default HomeRoute;
