import React from 'react';
// Semantic UI CSS
import { Card, Image, Icon, Rail } from 'semantic-ui-react';

// Custom Component Import..
import QuickScore from '../../../../CustomElements/QuickScore';
import SnSegment from '../../../../CustomElements/SnSegment';

// Images
import cricketImg1 from './images/cricket-india-pune-england-1st-odi-at_4f5b4518-db07-11e6-a473-4fa7bf176867.JPG';
import bcci from './images/bcci.png';
import australia from './images/australia.jpg';

function RecentMatchCard() {
  return (
    <Card>
      {/* https://stackoverflow.com/questions/18339549/floating-div-over-an-image/18339713 */}
      <div
        style={{
          position: 'relative',
        }}
      >
        <Image src={cricketImg1} />
        <SnSegment
          style={{ color: '#ffffff', borderWidth: 0, paddingBottom: '0' }}
          bottom
          fluid
          textGradient
          rounded={false}
        >
          <QuickScore
            data={{
              a: {
                title: 'IND',
                subtitle: '152/3 (24.5)',
                logo: bcci,
                logoClassName: [],
                active: true,
              },
              b: {
                title: 'AUS',
                subtitle: '24/3 (24.5)',
                logo: australia,
                active: false,
              },
            }}
          />
        </SnSegment>
      </div>

      <Card.Content>
        <Card.Header>Matthew</Card.Header>
        <Card.Meta>
          <span className="date">Joined in 2015</span>
        </Card.Meta>
        <Card.Description>Matthew is a musician living in Nashville.</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <a>
          <Icon name="user" />
          22 Friends
        </a>
      </Card.Content>
    </Card>
  );
}

export default RecentMatchCard;
