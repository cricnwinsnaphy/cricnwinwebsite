import React from 'react';

// Semantic UI CSS
import { Card } from 'semantic-ui-react';

// Custom Component..
import RecentMatchCard from '../RecentMatchList/RecentMatchCard';

function RecentMatchList() {
  return (
    <div
      style={{
        marginTop: '40px',
      }}
    >
      <div
        style={{
          marginBottom: '20px',
        }}
      >
        Recent Matches
      </div>
      <Card.Group itemsPerRow={3}>
        <RecentMatchCard />
        <RecentMatchCard />
        <RecentMatchCard />
        <RecentMatchCard />
        <RecentMatchCard />
        <RecentMatchCard />
      </Card.Group>
    </div>
  );
}

export default RecentMatchList;
