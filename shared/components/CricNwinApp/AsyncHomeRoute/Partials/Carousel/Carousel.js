import React from 'react';
// Slider..
import Slider from 'react-slick';
// CSS..
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import CarouselItem from './CarouselItem';

// Images
import indVsAust from './images/india-vs-australia.png';
import bcci from './images/bcci.png';
import australia from './images/australia.jpg';
// Image Carousel...
import cricket from './images/cricket.png';

function Carousel() {
  const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
  };
  return (
    <Slider {...settings}>
      <div>
        <CarouselItem
          data={{
            status: 'live',
            heading: 'SOUTH AFRICA TOUR OF ENGLAND 2018',
            backgroundImage: indVsAust,
            quickScore: {
              a: {
                title: 'IND',
                subtitle: '152/3 (24.5)',
                logo: bcci,
                active: true,
              },
              b: {
                title: 'AUS',
                subtitle: '24/3 (24.5)',
                logo: australia,
                active: false,
              },
            },
            lastSixScore: {
              title: 'England won by 6 runs',
              subtitle: 'Last 6 balls',
              labels: [
                {
                  color: 'default',
                  value: '1',
                  key: '0',
                },
                {
                  color: 'red',
                  value: 'W',
                  key: '1',
                },
                {
                  color: 'teal',
                  value: '4',
                  key: '2',
                },
                {
                  color: 'teal',
                  value: '4',
                  key: '3',
                },
                {
                  color: 'teal',
                  value: '6',
                  key: '4',
                },
                {
                  color: 'default',
                  value: '1',
                  key: '5',
                },
              ],
            },
            actions: [
              {
                title: 'Play & Win',
                onClick() {
                  console.log('I am clicked.');
                },
                key: '0',
              },
            ],
          }}
        />
      </div>
      <div>
        <CarouselItem
          data={{
            status: 'delayed',
            heading: 'CRICKET DEMO SCREEN',
            backgroundImage: cricket,
            quickScore: {
              a: {
                title: 'IND',
                subtitle: '152/3 (24.5)',
                logo: bcci,
                active: true,
              },
              b: {
                title: 'AUS',
                subtitle: '24/3 (24.5)',
                logo: australia,
                active: false,
              },
            },
            lastSixScore: {
              title: 'England won by 6 runs',
              subtitle: 'Last 6 balls',
              labels: [
                {
                  color: 'default',
                  value: '1',
                  key: '0',
                },
                {
                  color: 'red',
                  value: 'W',
                  key: '1',
                },
                {
                  color: 'teal',
                  value: '4',
                  key: '2',
                },
                {
                  color: 'teal',
                  value: '4',
                  key: '3',
                },
                {
                  color: 'teal',
                  value: '6',
                  key: '4',
                },
                {
                  color: 'default',
                  value: '1',
                  key: '5',
                },
              ],
            },
            actions: [
              {
                title: 'Earn',
                onClick() {
                  console.log('I am clicked. 2');
                },
                key: '1',
              },
              {
                title: 'Play',
                onClick() {
                  console.log('I am clicked. 2');
                },
                key: '2',
              },
            ],
          }}
        />
      </div>
    </Slider>
  );
}

export default Carousel;
