import React from 'react';

// Semantic UI CSS
import { Segment, Image, Rail, Label, Item, List, Button, Grid } from 'semantic-ui-react';

// Custom Element
import QuickScore from '../../../../CustomElements/QuickScore';
import SnSegment from '../../../../CustomElements/SnSegment';
import LastSixScore from '../../../../CustomElements/LastSixScore';

function CarouselItem({ data }) {
  return (
    <Segment style={{ padding: '0' }}>
      <Image style={{ height: '350px' }} fluid src={data.backgroundImage} />
      <Rail style={{ width: '100%' }} attached internal position="left">
        <SnSegment
          style={{ color: '#ffffff', borderWidth: '0px', backgroundColor: 'transparent' }}
          fluid
          rounded={false}
        >
          <Label
            style={{ borderRadius: 0, fontSize: '10px', textTransform: 'uppercase' }}
            color={'red'}
            key={'red'}
          >
            {data.status}
          </Label>

          <Label
            style={{
              backgroundColor: '#2c9fa6',
              borderColor: '#2c9fa6',
              color: '#ffffff',
              position: 'absolute',
              right: 0,
              borderRadius: 0,
              marginRight: 0,
              paddingRight: 0,
              fontSize: '10px',
              paddingRight: '20px',
              textTransform: 'uppercase',
            }}
            key={'teal'}
          >
            {data.heading}
          </Label>
        </SnSegment>
      </Rail>
      <SnSegment
        style={{ color: '#ffffff', borderWidth: 0, paddingBottom: '0' }}
        bottom
        fluid
        textGradient
        rounded={false}
      >
        <Segment.Group
          style={{
            margin: '0',
            borderRadius: 0,
            padding: '0',
            borderWidth: 0,
            backgroundColor: 'transparent',
          }}
          horizontal
        >
          <Segment style={{ paddingLeft: '0', paddingRight: '0' }}>
            <Grid verticalAlign="middle">
              <Grid.Column
                width={7}
                textAlign="center"
                style={{
                  paddingRight: '0',
                }}
              >
                <div
                  style={{
                    borderRightWidth: '1px',
                    borderRightStyle: 'solid',
                    borderRightColor: '#fff',
                  }}
                >
                  {/* Quick Score Card */}
                  <QuickScore data={data.quickScore} />
                </div>
              </Grid.Column>
              <Grid.Column width={5} textAlign="center">
                <div
                  style={{
                    borderRightWidth: '1px',
                    borderRightStyle: 'solid',
                    borderRightColor: '#fff',
                    paddingLeft: '0',
                  }}
                >
                  {/* Last Six Score */}
                  <LastSixScore data={data.lastSixScore} />
                </div>
              </Grid.Column>
              <Grid.Column
                style={{
                  paddingLeft: 0,
                  paddingRight: 0,
                }}
                width={4}
                textAlign="center"
              >
                <div>
                  {getButtons(data.actions)}
                </div>
              </Grid.Column>
            </Grid>
          </Segment>
        </Segment.Group>
      </SnSegment>
    </Segment>
  );
}

const getButtons = function (actions) {
  const componentList = [];
  if (actions) {
    if (actions.length) {
      actions.forEach((action, index) => {
        if (action) {
          componentList.push(
            <span
              key={action.key}
              style={{
                marginRight: '20px',
              }}
            >
              <Button
                key={action.key}
                onClick={action.onClick}
                circular
                className="cricnwin-light-pink-button"
              >
                {action.title}
              </Button>
            </span>,
          );
        }
      });
    }
  }

  return componentList;
};

export default CarouselItem;
