import React from 'react';
import PropTypes from 'prop-types';

// Semantic UI CSS
import { List, Image, Label } from 'semantic-ui-react';
/* Quick Score Format */
function QuickScore({ data }) {
  return (
    <List horizontal verticalAlign="middle">
      <List.Item>
        <List.Content style={{ display: 'inline-block' }}>
          {/* TITLE */}
          <List.Header style={{ color: '#ffffff', textAlign: 'right' }}>
            {data.a.title}
            {isActive(data.a.active)}
          </List.Header>
          {/* Sub-Title */}
          <List.Description
            style={{
              color: '#ffffff',
              textAlign: 'right',
              fontSize: '0.8em',
            }}
          >
            {data.a.subtitle}
          </List.Description>
        </List.Content>
      </List.Item>
      <List.Item>
        <Image
          style={{
            width: '45px',
            height: 'auto',
          }}
          avatar
          src={data.a.logo}
        />
      </List.Item>
      <List.Item>
        <Label circular size="tiny" color="red" key="red">
          vs
        </Label>
      </List.Item>
      <List.Item>
        <Image
          style={{
            width: '45px',
            height: 'auto',
          }}
          avatar
          src={data.b.logo}
        />
      </List.Item>
      <List.Item>
        <List.Content style={{ display: 'inline-block' }}>
          <List.Header style={{ color: '#ffffff', textAlign: 'left' }}>
            {isActive(data.b.active)}
            {data.b.title}
          </List.Header>
          <List.Description
            style={{
              color: '#ffffff',
              textAlign: 'right',
              fontSize: '0.8em',
            }}
          >
            {data.b.subtitle}
          </List.Description>
        </List.Content>
      </List.Item>
    </List>
  );
}

/**
 * Return Asterick is team is active right now..
 * @param {*} isActive
 */
const isActive = function (isActive) {
  if (isActive) {
    return <List.Icon style={{ marginRight: 0 }} name="asterisk" verticalAlign="top" size="tiny" />;
  }
};

// Required Prop Types for the Dumb Components..
QuickScore.propTypes = {
  data: PropTypes.shape({
    a: PropTypes.shape({
      title: PropTypes.string.isRequired,
      subtitle: PropTypes.string,
      logo: PropTypes.string.isRequired,
      active: PropTypes.bool,
    }).isRequired,
    b: PropTypes.shape({
      title: PropTypes.string.isRequired,
      subtitle: PropTypes.string,
      logo: PropTypes.string.isRequired,
      active: PropTypes.bool,
    }).isRequired,
  }).isRequired,
};

export default QuickScore;
