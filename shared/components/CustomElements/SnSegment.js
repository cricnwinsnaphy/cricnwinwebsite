import React from 'react';
// Semantic UI CSS
import { Segment } from 'semantic-ui-react';

/**
 * Improved Version of Semantic Segment
 * With Option for Bottom.
*/
class SnSegment extends React.Component {
  render() {
    const bottomStyle = {
      position: 'absolute',
      bottom: '0',
    };

    const props = { ...this.props };

    let style = {};

    // Will provide text gradient..
    if (this.props.textGradient) {
      style.background =
        'linear-gradient(rgba(0, 0, 0, -0.4), rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.9))';
      delete props.textGradient;
    }

    if (this.props.bottom) {
      style = {
        ...bottomStyle,
        ...style,
      };

      delete props.bottom;
    }

    if (this.props.rounded === false) {
      style.borderRadius = '0';
      delete props.rounded;
    }

    if (this.props.fluid) {
      style.width = '100%';
      delete props.fluid;
    }

    if (props.style) {
      style = { ...style, ...props.style };
    }

    return (
      <Segment {...props} style={style}>
        {this.props.children}
      </Segment>
    );
  }
}

export default SnSegment;
