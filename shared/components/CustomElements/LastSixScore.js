import React from 'react';
import PropTypes from 'prop-types';

// Semantic UI CSS
import { List, Label } from 'semantic-ui-react';
import map from 'lodash/map';

const LastSixScore = function ({ data: { title, subtitle, labels } }) {
  labels = labels || [];
  return (
    <List>
      <List.Item>
        {title}
      </List.Item>
      <List.Item>
        <List horizontal>
          <List.Item>
            <span
              style={{
                color: '#ffffff',
                fontSize: '0.8em',
              }}
            >
              {subtitle}
            </span>
          </List.Item>
          <List.Item>
            {getLabels(labels)}
          </List.Item>
        </List>
      </List.Item>
    </List>
  );
};

/**
 * WIll return a list of labels..
 * @param {*} labels
 */
const getLabels = function (labels) {
  const componentList = [];
  if (labels) {
    if (labels.length) {
      labels.forEach((label) => {
        if (label) {
          let labelComp;
          if (label.color === 'default') {
            labelComp = (
              <Label size="tiny" key={label.key} circular>
                {' '}{label.value}{' '}
              </Label>
            );
          } else {
            labelComp = (
              <Label size="tiny" circular key={label.key} color={label.color}>
                {' '}{label.value}{' '}
              </Label>
            );
          }

          componentList.push(labelComp);
        }
      });
    }
  }

  return componentList;
};

// Required Prop Types for the Dumb Components..
LastSixScore.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    labels: PropTypes.array.isRequired,
  }).isRequired,
};

export default LastSixScore;
